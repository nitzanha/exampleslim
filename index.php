<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging());


//----------------------------------------------
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});

$app->get('/customers/{cnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber']);
});

$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber' ]. ' Product '.$args['pnumber' ]);
});

//----------------------------------------------
//------------GET users------------------------
$app->get('/users', function($request, $response, $args){
    $_user = new User(); //create new user
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
        $payload[$usr->id] = [
            'name'=>$usr->name,
            'phoneNumber'=>$usr->phoneNumber
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});
//----------------------------------------------
//---------------CREATE users-----------------------

$app->post('/users', function($request, $response, $args){
       $name = $request->getParsedBodyParam('name','');
       $phoneNumber = $request->getParsedBodyParam('phoneNumber','');
       $_user = new User();
        $_user->name = $name;
        $_user->phoneNumber = $phoneNumber;
        $_user->save();
        if($_user->id){
            $payload = ['user id: '=>$_user->id];
            return $response->withStatus(201)->withJson($payload);
        }
        else{
            return $response->withStatus(400);
        }
     });

     //----------------------------------------------
     //--------------DELETE user-------------------------
     $app->delete('/users/{id}', function($request, $response,$args){
        $_user = User::find($args['id']);
        $_user->delete();
        if($_user->exists){
            return $response->withStatus(400);
        }
        else{
             return $response->withStatus(200);
        }
    });

    //----------------------------------------------
    //-----------GET user--------------------------
    $app->get('/users/{id}', function($request, $response,$args){
        $_id=$args['id'];
        $user = User::find($_id);
       return $response->withStatus(200)->withJson($user);
    });

    //----------------------------------------------
    //-------------UPDATE  user--------------------------
    $app->put('/users/{id}', function($request, $response, $args){
        $username = $request->getParsedBodyParam('username','');
        $userphonenumber = $request->getParsedBodyParam('userphonenumber','');
        $_user = User::find($args['id']);
        //die("user id is " . $_user->id);
        $_user->name = $username;
        $_user->phonenumber = $userphonenumber;
        if($_user->save()){
            $payload = ['id'=>$_user->id,"result"=>"The user has been updated successfuly"];
            return $response->withStatus(200)->withJson($payload);
        }
        else{
             return $response->withStatus(400);
        }
    });

    //----------------------------------------------

$app->add(function ($req, $res, $next) {
        $response = $next($req, $res);
        return $response
                ->withHeader('Access-Control-Allow-Origin', '*')
                ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    });

    
$app->run();